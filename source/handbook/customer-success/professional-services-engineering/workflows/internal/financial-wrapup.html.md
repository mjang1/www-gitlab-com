---
layout: markdown_page
title: Financial Wrap-up
category: Internal
---

Once the SOW items have been delivered in full:
1. Send the [project summary e-mail](/handbook/customer-success/professional-services-engineering/workflows/project_execution/project-summary.html)
1. Follow the steps on that page
1. Once completed, Chatter (in Salesforce) the following individuals: Controller (Wilson), Manager, Profesional Services (Brendan), Accounts Payable (Melissa).
1. Ensure that the engagement folder in Google Drive is up to date with all relative documentation.
