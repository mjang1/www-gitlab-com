---
layout: handbook-page-toc
title: "Merchandise handling"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Daily routine

One of the main community advocate task is to fulfill all the swag orders.

### Swag orders

Advocates should check for the new orders regularly as the orders should be fulfilled and shipped in a timely manner. In order to process these orders you'll have to fulfill them both in Shopify and Printfection.

[Video tutorial](https://drive.google.com/file/d/16pcdtfwbNye0SmtX20QOhqwP0TYbijVG/view?usp=sharing) available for GitLab team members.

#### Printfection

Fulfilling in Printfection means that Prinfection will pack and ship the items.

1. Login to Printfection
2. Go to the "Shopify store orders" collection under the campaign tab.
3. Go to the manage tab
4. Make sure that all the orders from Shopify are shown in there and that everything is okay.
5. Press the "place the order button".

That's all, Printfection will handle the rest. 

Note: Printfection also sends the email confirmation to the customer.

#### Shopify

Fulfilling in Shopify serves to send the actual notification that an order has been processed, including the invoice for the customer.

1. Login to Shopify
2. Go to the orders page
3. Make sure to check all the new orders highlighted with the yellow Unfulfilled tag.
4. Click on actions > fulfill selected orders
5. Make sure that the default "send a notification" option is selected and press the fulfill button.

That's all, the customers should receive their confirmations automatically.

### Adding new products to the swag store

If you need help adding new products to the shop, please follow this [video tutorial](https://drive.google.com/file/d/1IHSbanrGokYCKo7O4zJ8p408JsHbZRmi/view?usp=sharing) or the steps bellow:

1. Gather item inventory data - contact the product's vendor
1. Log in to Shopify
1. Open the products page
   - Click the Add Product button
   - Fill out information about the item
   - If you don't have the information for the description, please ask/search for it and be careful - the info could be sensitive
   - Image is important, contact the product vendor for the high-rez photo.
   - Fill out the price for the item
   - Select "Shopify tracks this product's inventory"
   - Enter the weight of the product if that info is available.
   - Before saving the product, please check search engine listing preview

<i class="fas fa-info-circle" aria-hidden="true" style="color: rgb(49, 112, 143)
;"></i> For more information, see this [official guide](https://help.shopify.com/en/manual/products/add-update-products)
{: .alert .alert-info}

### Creating the discount codes

In order to create a coupon code on Shopify, please check out this [video tutorial](https://drive.google.com/file/d/1h9ZJgktR2iGxJqz7Fqy9FqMjVtF19YJ9/view?usp=sharing).

### Monthly report sharing

Each month the advocates have to run a report of the sold swag items. The reports needs to be sent to Kim S. who is our Accounting Manager.

Please follow the steps in the [video tutorial here](https://drive.google.com/file/d/1TiVxM9an0SIFrqp7ESj4QiqIj0ekAZ3F/view?usp=sharing) in order to accomplish this task.

### Updating the contributors giveaway sheet 

We are sharing swag giveaway links to the community contributors. Advocates should replenish the links each quarter and update the with the new links [sheet](https://docs.google.com/spreadsheets/d/1kzK25SE9jPXx50Lw0W8jtm9u77Zr4w13FAg1gyD6Egk/edit#gid=1578711076).

Please check out this [video tutorial](https://drive.google.com/file/d/1uggqOMkywNbqoPBJjkjAAJAcGz-HQjZH/view?usp=sharing) no how to update the sheets or follow the steps bellow:

1. Login to Printfection.
2. Go to "Community Contributor" giveaway campaign.
3. Click on the Manage tab.
4. Generate new links.
5. Make sure to download the CSV file of the freshly created links only
6. Upload the csv file or copy/paste the new links into the [sheet](https://docs.google.com/spreadsheets/d/1kzK25SE9jPXx50Lw0W8jtm9u77Zr4w13FAg1gyD6Egk/edit#gid=1578711076)
